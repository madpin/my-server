

fusermount -u ~/media_mount/movies || true
fusermount -u ~/media_mount/tv || true

rclone mount --allow-other --daemon --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off sgdu:/media ~/media_mount
