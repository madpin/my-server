#!/bin/bash

# docker-compose down

# docker network create -d bridge media_net || true
# docker network connect media_net nginx-proxy || true

# mkdir ~/media/movies -p
# mkdir ~/media/tv -p
# mkdir ~/media/transmission -p

# mkdir ~/media_mount/movies -p
# mkdir ~/media_mount/tv -p
# mkdir ~/media_mount/masterclass -p
# mkdir ~/media_mount/raw -p

# pkill -f "rclone*.*madpin/media_mount/"

# fusermount -u ~/media_mount/movies || true
# fusermount -u ~/media_mount/tv || true
# fusermount -u ~/media_mount/masterclass || true
# fusermount -u ~/media_mount/raw || true
# fusermount -u /mnt/sgdu || true
# fusermount -u /mnt/pcloud || true


# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off sgdu:/media/movies ~/media_mount/movies &
# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off sgdu:/media/tv ~/media_mount/tv &
# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off sgdu:/media/raw ~/media_mount/raw &
# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode full p:/Masterclass ~/media_mount/masterclass &

# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off sgdu: /mnt/sgdu &
# # rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode off p: /mnt/pcloud &



# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full sgdu:/media/movies ~/media_mount/movies
# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full sgdu:/media/tv ~/media_mount/tv
# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full sgdu:/media/raw ~/media_mount/raw
# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full p:/Masterclass ~/media_mount/masterclass
# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full sgdu: /mnt/sgdu
# rclone mount --daemon --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 10G --buffer-size 512M --transfers 8 --vfs-cache-mode full p: /mnt/pcloud

# sleep 5

# docker-compose up -d
