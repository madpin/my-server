docker-compose down

mkdir ~/plex/tvseries/Masterclass -p
mkdir ~/plex/movies/pcloud -p
mkdir ~/mount_sgdu -p

fusermount -u ~/plex/tvseries/Masterclass || true
fusermount -u ~/plex/movies/pcloud || true
fusermount -u ~/mount_sgdu || true

rclone mount --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode full p:/Masterclass ~/plex/tvseries/Masterclass &
rclone mount --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode full p:/Movies ~/plex/movies/pcloud &
rclone mount --allow-other --dir-cache-time 48h --vfs-read-chunk-size 32M --vfs-read-chunk-size-limit 2G --buffer-size 512M --vfs-cache-mode full sgdu: ~/mount_sgdu &
sleep 5
docker-compose up -d
