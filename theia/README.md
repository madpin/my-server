# Theia and Httpd Password

I had to implement `jrcs/letsencrypt-nginx-proxy-companion` in the proxy.

And will run:


```bash
sudo yum install httpd-tools
sudo mkdir -p /etc/nginx/htpasswd
sudo touch /etc/nginx/htpasswd/theia.madpin.dev
sudo htpasswd /etc/nginx/htpasswd/theia.madpin.dev madpin
```


In the first run, after restart:

```bash
git config --global user.email "computer@madpin.dev"
git config --global user.name "MadPin.dev"
```