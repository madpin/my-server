#!/bin/bash

echo "calibre"
cd calibre
docker-compose pull
docker-compose up -d
cd ..

echo "diun"
cd diun
docker-compose pull
docker-compose up -d
cd ..

echo "jupyter"
cd jupyter
docker-compose pull
docker-compose up -d
cd ..

echo "media_server"
cd media_server
docker-compose pull
docker-compose up -d
cd ..

echo "proxy"
cd proxy
docker-compose pull
docker-compose up -d
cd ..


echo "vscode"
cd vscode
docker-compose pull
docker-compose up -d
cd ..

echo "Proxy Network"
docker network create -d bridge media_net || true
docker network connect media_net nginx-proxy || true
