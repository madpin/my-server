#!/bin/bash

adduser madpin
usermod -aG wheel madpin

su madpin <<EOF
mkdir ~/.ssh 2> /dev/null
chmod 0700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 0644 ~/.ssh/authorized_keys
echo "" | ssh-keygen -t rsa -N ""
cat >~/.ssh/authorized_keys <<EOL
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAzcDSyz6XauBQO+ntednOAB8fDUWV283ej9CbLLD5Sn7hXmGXVNg+1WdXkd7koDJozU05v5BrosseWKmjUztdf+F5wiYegxni1OK42+22rti351x/l8RXFy/xoKUO9jeS/D3/HctiFlZRxdM3MZc7c3KOMRVtBdqD5AxjgLP5dwyOakSTx7WHrwycTpXSo21GyQPtH+g0s9+LvN28mDHfwh8v8ugHY+vo/jQYgwI4wmLVbYqB7WdsWzD+z5H58ldys+eXXFE56CACEE4U0WUTNnYCr9M/fJpHfjnAJfs+JDn0H9yE4Ol4Enm7Oh1BFyGeN4nWsx7ptfJbDcasgpeX8a1d+IJgnpffihVtJh9wUb8RXZYgJSXW73LvDAmIEiHddpHrE5z8TGFNdZGrD9xY9DNKvFYwVXzrntPHK8VqpBkxD2Z6d5zi610lpL/qBqjuwtU6hVgrz++jDKdqSaOBZ4PD5E/qbOVa2MEgfnOHIZnysjfOPk5FcwJM7moNrVVUbC1X+0xTNF/Jn0IZJ8wNTRTU7eJ7hAuFV/Y39RS6QmhEmU4qRrpLwdvnVCENAawnmZoU1jtoH87dSk/PvzICEi2fvofXlgxg05zsVxjcDU9xSH9vjo/ScbFlwEUHhTc5Sxl3/slh/IHA2EF89toLfosC6HjnaF2T6Ve/kRuTCUE= rsa-key-20170509
EOL
cat >>~/.ssh/authorized_keys <<EOL
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2tyjvLAEYfSULkQ31h98K5w9fPLUVb+Mz+QZDx81WNuv8MqjdvkUrekJY9FyEd/i0k3ALO4IoFTXVuMvZZaqDTUgWzHsdxfpWIx87VlzPJGPpJTZ4lacESGbOKIFNV16KjeqRNnZa0ChAdB+wUxT2MMgDm1zllJx/N8S7zvF6mgzYsUeSpYQygHQ/rKBO9r13VOsTxZOaEw8ZosNjtCUX8CHn0yb9ovzLU5kjgsu2ekYwBxr6gChPzXYnK2RrR+AtImfmiDPUvBAAAjAPq01bjhDfC1l9S9dTTvcF2GPQ9CEUtR+iviuJ0CTBC4rkJlt70dkkzj/Qr8PeIIZ9OTwR tpinto@indeed.com
EOL
cat ~/.ssh/id_rsa.pub >>  ~/.ssh/authorized_keys
EOF

# sed -c -i "s/\(UsePAM *\s *\).*/\1no/" /etc/ssh/sshd_config
sed -c -i "s/\(PasswordAuthentication *\s *\).*/\1no/" /etc/ssh/sshd_config

sed -i '/PubkeyAuthentication/s/^#//g' /etc/ssh/sshd_config # uncomment
sed -i '/wheel.*NOPASSWD: ALL/s/^#//g' /etc/sudoers # uncomment


#sed -i '/<pattern>/s/^/#/g' file #comment
systemctl restart  sshd

logout