#!/bin/bash


# Mysql
# Latest Version
# Open for Remote
sudo docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=senha -e MYSQL_ROOT_HOST=% -d mysql:latest


# Postgres
# Latest Version
# Open for Remote
sudo docker run -p 5432:5432 --name postgres -e POSTGRES_PASSWORD=senha -d postgres

# Netdata
# Monitoring
# vps.madpin.net:19999
docker run -d --name=netdata   -p 19999:19999   -v /proc:/host/proc:ro   -v /sys:/host/sys:ro   -v /var/run/docker.sock:/var/run/docker.sock:ro   --cap-add SYS_PTRACE   --security-opt apparmor=unconfined   netdata/netdata
